from setuptools import setup

setup(
    name='ptsstorage',
    version='0.1.0',
    author='Irina", "Kapler',
    author_email='irkapler@gmail.com',
    packages=['ptsstorage'],
    url='https://bitbucket.org/rtsvizteam/ptsstorage',	
    license='LICENSE',
	description='Python package to access data stored by "rtsstorage" R package.',
    long_description=open('README.md').read(),
    install_requires=[
        "pandas",
        "pyarrow",
		"pymongo",
    ],	
)


