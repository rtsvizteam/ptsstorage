ptsstorage
====


A companion package for the R 'rtsstorage' package
===


The `ptsstorage` package provides access to the data storage in file and database format.


Installation:
===


To install the development version run following code:


```
pip install git+ssh://git@bitbucket.org/rtsvizteam/ptsstorage.git
```
	

Example :
===

```python
	import ptsstorage as pts

	# -------------------------
	# Example File Storage
	# assume 'rtsstorage' package save data at 'c:\temp\yahoo_parquet' folder
	# -------------------------
	stg = pts.FileStorage(r'c:\temp\yahoo_parquet')
	stg.list()
	stg.exists('ibm')

	a = stg.load('ibm')
	stg.save(a,'ibm1')
	stg.list()

	stg.delete('ibm1')
	stg.list()

	del stg


	# -------------------------
	# Example Database Storage
	# assume 'rtsstorage' package save data at 'mongodb://localhost' mongodb under 'data_storage' label
	# -------------------------
	stg = pts.MongoDBStorage('mongodb://localhost', 'data_storage')
	stg.list()
	stg.exists('IBM')

	a = stg.load('IBM')
	stg.save(a,'IBM1')
	stg.list()


	stg.delete('IBM1')
	stg.list()
	
	del stg	
		
```
