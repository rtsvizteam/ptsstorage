
import glob
import os

import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq	

def data2arrow(d):
    # table = pa.Table.from_pandas(b)
    # date conversion pd.to_datetime(b.index)
    arrays = [pa.array(d.index.date)] +  [pa.array(d[i]) for i in d.columns]
    out = pa.Table.from_arrays(arrays, names=['name'] + d.columns.to_list()) 
    return out

def arrow2data(a):	
    out = a.select(a.column_names[1:]).to_pandas()
    out.index = a[a.column_names[0]]
    return out

	
class FileStorage:
	location = ''
	extension = 'parquet'

	def __init__(self, location):
		self.location = location

	#def __del__(self):

	def load(self, id):
		i = self.ticker(id)
		out = pq.read_table(i)
		return arrow2data(out)

	def save(self, data, id):
		i = self.ticker(id)
		out = data2arrow(data)
		pq.write_table(out, i)
	
	def delete(self, id):
		i = self.ticker(id)
		if os.path.exists(i):
			os.remove(i)
  
	def exists(self, id):
		return os.path.exists(self.ticker(id))
	
	def list(self):
		files = glob.glob(os.path.join(self.location, '*.{}'.format(self.extension)))
		return [os.path.splitext(os.path.basename(i))[0] for i in files]

	def ticker(self, id):
		return os.path.join(self.location, '{}.{}'.format(id,self.extension))


from pymongo import MongoClient
from gridfs import GridFS

class MongoDBStorage:
	fs = ''
	tag = 'yahoo'
	
	def __init__(self, url = 'mongodb://localhost', db = 'data_storage'):
		self.url = url
		#client = MongoClient(mongodb_conn_str, username = username, password = password)
		client = MongoClient(url)    
		db = client[db]
		self.fs = GridFS(db)		

	#def __del__(self):
		
	def load(self, id):
		i = self.ticker(id)		
		file = self.fs.find_one({"filename": i})
		data = file.read()
		buf = pa.BufferReader(data)
		out = pq.read_table(buf)
		return arrow2data(out)

		
	# [Python Examples of pyarrow.BufferReader](https://www.programcreek.com/python/example/126825/pyarrow.BufferReader)
	def save(self, data, id):
		i = self.ticker(id)
		out = data2arrow(data)
		buf = pa.BufferOutputStream()
		pq.write_table(out, buf)		
		# reader = pa.BufferReader(buf.getvalue().to_pybytes())
		self.fs.put(buf.getvalue().to_pybytes(), filename=i)

	def delete(self, id):
		i = self.ticker(id)
		if self.fs.exists({"filename": i})	:
			#file = self.fs.find_one({"filename": i})
			#self.fs.delete(file._id)
			for file in self.fs.find({"filename": i}):
				self.fs.delete(file._id)
			
		
	def exists(self, id):
		return self.fs.exists({"filename": self.ticker(id)})	

	def list(self):
		return self.fs.list()
		
	def ticker(self, id):
		return '{}|{}'.format(self.tag, id)



